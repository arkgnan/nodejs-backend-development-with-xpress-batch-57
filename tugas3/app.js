// soal 1
const x = 10;
const y = 20;
let sum = x + y;
console.log(sum);

// soal 2
/*
* var
* - Tidak ada batasan scope variabel
* - Dapat dideklarasikan ulang 
* - Dapat diubah nilainya
* let
* - memiliki scope blok, hanya dapat diakses didalam block code dimana variable tersebut dideklarasikan
* - tidak dapat dideklarasikan ulang
* - dapat diubah nilainya
* const
* - memiliki scope blok, hanya dapat diakses didalam block code dimana variable tersebut dideklarasikan
* - Daat dideklarasikan harus di inisialisasi terlebih dahulu
* - dapat diubah nilainya, namun property dalam object yang dideklarasikan dapat diubah
*/

// soal 3
for (let i = 1; i <= 5; i++) {
    console.log(i);
}

// soal 4
let i = 5;

while (i >= 1) {
    console.log(i);
    i--;
}

// soal 5
function greet(name) {
    console.log(`Hello, ${name}!`);
}
greet("John"); // Output: Hello, John!

// soal 6
function add(a, b) {
    return a + b;
}

// soal 7
let fruits = ["Pepaya", "Mangga", "Pisang"];
fruits.push("Jambu");
console.log(fruits);

// soal 8
let numbers = [1, 2, 3, 4, 5];
let doubledNumbers = numbers.map(number => number * 2);
console.log(doubledNumbers); // Output: [2, 4, 6, 8, 10]

// soal 9
let person = {
    name: "John",
    age: 30
};
person.occupation = "Developer";
console.log(person);

// soal 10
let people = [
    { name: "John", age: 30 },
    { name: "Jane", age: 20 },
    { name: "Bob", age: 35 }
];

let adults = people.filter(person => person.age > 25);
console.log(adults); // Output: [{ name: "John", age: 30 }, { name: "Bob", age: 35 }]