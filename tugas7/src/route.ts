import express from "express";

import { single, multiple } from "./middlewares/upload.middleware";
import { handleUpload } from "./utils/cloudinary";

const router = express.Router();

router.post("/upload/single", single, async (req, res) => {
    try {
        if (req.file) {
            console.log(req.file)
            const result = await handleUpload(req.file.buffer) as any;
            const url = result?.secure_url;
            res.status(201).json({ image_url: url });
        } else {
            res.status(400).json({ error: "No file uploaded" });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: "Error uploading image to Cloudinary" });
    }
});
router.post("/upload/multiple", multiple, async (req, res) => {
    try {
        console.log(req.files)
        if (req.files) {
            const urls = Object.values(req.files).map(async (file) => {
                const result = await handleUpload(file.buffer) as any;
                const url = result?.secure_url;
                return { image_url: url };
            });
            Promise.all(urls).then((urls) => {
                res.status(201).json(urls);
            });
        } else {
            res.status(400).json({ error: "No file uploaded" });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: "Error uploading image to Cloudinary" });
    }
});

export default router;
