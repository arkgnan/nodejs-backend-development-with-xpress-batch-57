import express from "express";
import route from "./route";

const app = express();
const PORT = 3000;
app.use(express.urlencoded({ extended: true }))
app.use("/api", route);

app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}`);
});
