const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());

let products = [
    { id: 1, name: 'Laptop', category: 'Perabotan' }, 
    { id: 2, name: 'Meja', category: 'Perabotan' }
];

let categories = [
    { id: 1, name: 'Elektronik' }, 
    { id: 2, name: 'Perabotan' }
];

// GET all categories
app.get('/api/categories', (req, res) => {
  res.json(categories);
});

// GET category by id
app.get('/api/categories/:id', (req, res) => {
  const categoryId = parseInt(req.params.id);
  const category = categories.find(c => c.id === categoryId);
  if (category) {
    res.json(category);
  } else {
    res.status(404).json({ message: 'Category not found' });
  }
});

// POST new category
app.post('/api/categories', (req, res) => {
  const newCategory = req.body;
  newCategory.id = categories.length ? categories[categories.length - 1].id + 1 : 1;
  categories.push(newCategory);
  res.status(201).json(newCategory);
});

// PUT update category
app.put('/api/categories/:id', (req, res) => {
  const categoryId = parseInt(req.params.id);
  const categoryIndex = categories.findIndex(c => c.id === categoryId);
  if (categoryIndex !== -1) {
    categories[categoryIndex] = { id: categoryId, ...req.body };
    res.json(categories[categoryIndex]);
  } else {
    res.status(404).json({ message: 'Category not found' });
  }
});

// DELETE category by id
app.delete('/api/categories/:id', (req, res) => {
  const categoryId = parseInt(req.params.id);
  categories = categories.filter(c => c.id !== categoryId);
  res.status(204).send();
});

// GET products by name
app.get('/api/products', (req, res) => {
    // check if has query parameter
    const query = req.query.name;
    if (!query) {
        res.json(products);
        return;
    }
    // search products by name
    const productsByName = products.filter(product => product.name.toLowerCase().includes(query.toLowerCase()));
    res.json(productsByName);
});

// GET product in category
app.get('/api/categories/:id/products', (req, res) => {
    const categoryId = parseInt(req.params.id);
    const category = categories.find(c => c.id === categoryId);
    if (category) {
        const productsInCategory = products.filter(product => product.category === category.name);
        const query = req.query.name;
        if (!query) {
            res.json(productsInCategory);
            return
        }
        const productsByName = productsInCategory.filter(product => product.name.toLowerCase().includes(query.toLowerCase()));
        res.json(productsByName);
    } else {
        res.status(404).json({ message: 'Category not found' });
    }
});

app.listen(port, () => {
  console.log(`Server is running at <http://localhost>:${port}`);
});